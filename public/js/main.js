let case_type = document.querySelector('#case-type'); // get the case type field
let covid_status = document.querySelector('#covid-status'); // get the case type field

if (case_type) { // check if case type exists on page

    const showCaseStatus = () => {
        
        // get the hidden field (case status)
        let case_status_field = document.querySelector('.hidden-field'); 
        
        // display covid status field if case type is positive
        if (case_type.value == "Positive") {
            case_status_field.style.display = "block"; 
        } else {
            case_status_field.style.display = "none";
            covid_status.value = '';
        }

    }
    // run the function everytime the case type changes
    case_type.addEventListener('change', showCaseStatus); 

    // run the function on page load
    window.load = showCaseStatus(); 

}

// Awareness / Covid report show barangay based on selected city
let city = document.querySelector('#select-city'); // get the city field
let brgy = document.querySelector('#select-brgy'); // get the brgy field
let brgy_options = document.querySelectorAll('#select-brgy option'); // get all brgy field options
let generate_button = document.querySelector('#generate-report'); // get the generate button

if (city) { // check if city field exists on page

    const changeBrgyOptions = () => {

        // for each brgy options
        for (let brgy_option of brgy_options) {
            
            let brgy_city_id = brgy_option.getAttribute('city-data-id'); // get city-data-id (attribute) value
            
            // display brgy options related to city
            if (city.value == brgy_city_id) {
                brgy_option.style.display = 'block';
            } else {
                brgy_option.style.display = 'none';
                brgy.firstElementChild.style.display = 'block'; // always display the default value
            }
        }

    }

    const hideBrgyOptions = () => {
        
        // enable to generate if city field is not empty else disable generate
        if (city.value.length != 0) {

            // check if generate button exists
            if (generate_button) {
                generate_button.disabled = false; // enable generate button
            }
            
            brgy.disabled = false; // enable brgy field

        } else {
            // check if generate button exists
            if (generate_button) {
                generate_button.disabled = true; // disable generate button
            }
            if(brgy.value.length == 0) {
                brgy.disabled = true; // disable brgy field
            }
        }

        changeBrgyOptions();

    }
    
    const showBrgy = () => {
    
        // check if city is not empty
        if (city.value.length != 0) {

            // check if generate button exists
            if (generate_button) {
                generate_button.disabled = false; // enable generate button
            }

            brgy.disabled = false; // enable brgy field
            brgy.value = ''; // set brgy field value to empty

        } else {
            // check if generate button exists
            if (generate_button) {
                generate_button.disabled = true; // disable generate button
            }

            brgy.value = ''; // set brgy field value to empty
            brgy.disabled = true; // disable brgy field
        }
    
        changeBrgyOptions();
    
    }

    // run the function on page load
    window.load = hideBrgyOptions();

    // run the function everytime the city changes
    city.addEventListener('change', showBrgy);

}