<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brgy extends Model
{
    // Set relationship to cities table
    public function city() {
        return $this->belongsTo('App\City');
    }

    // Relationship many patients to one brgy
    public function patients() {
        return $this->hasMany('App\Patient');
    }
}
