<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    // Relationship many brgys to one city
    public function brgys() {
        return $this->hasMany('App\Brgy');
    }
}
