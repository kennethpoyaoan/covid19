<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    // Set relationship patient to brgy 
    public function brgy() {
        return $this->belongsTo('App\Brgy');
    }
}
