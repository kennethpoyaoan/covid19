<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Brgy;

class BrgysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // retrieve all barangays with cities
        $brgys = Brgy::orderBy('city_id', 'desc')->get();
        $cities = City::all();
        return view('brgys.index')->with(['brgys' => $brgys, 'cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // create new brgy
        $cities = City::all();
        return view('brgys.create')->with(['cities' => $cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form Validation
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required'
        ]);

        // Insert Brgy to database
        $brgy = new Brgy;
        $brgy->name = $request->input('name');
        $brgy->city_id = $request->input('city');
        $brgy->save();

        return redirect('/brgys')->with('success', 'Brgy added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // brgy single page
        $brgy = Brgy::find($id);
        $city = Brgy::find($id)->city;

        return view('brgys.show')->with(['brgy' => $brgy, 'city' => $city]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Edit existing brgy
        $brgy = Brgy::find($id);
        $cities = City::all();

        return view('brgys.edit')->with(['brgy' => $brgy, 'cities' => $cities]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Form Validation
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required'
        ]);

        // Create Brgy
        $brgy = Brgy::find($id);
        $brgy->name = $request->input('name');
        $brgy->city_id = $request->input('city');
        $brgy->save();

        return redirect('/brgys')->with('success', 'Brgy updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Delete existing brgy
        $brgy = Brgy::find($id);

        $brgy->delete();
        return redirect('/brgys')->with('success', 'Brgy removed successfully.');
    }
}