<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Brgy;
use App\Patient;
use DB;

class CovidReportsController extends Controller
{
    // Covid Reports Views
    public function index(Request $request) {

        $city_id = $request->input('city');
        $brgy_id = $request->input('brgy');

        $brgys = Brgy::all();
        $cities = City::all();
        
        $city_request = City::find($city_id);
        $brgy_request = Brgy::find($brgy_id);

        // results for city only
        $city_results_positive = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'patients.covid_status', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Positive']
                            ])->get();

        $city_results_active = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'patients.covid_status', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.covid_status', '=', 'Active']
                            ])->get();

        $city_results_recovered = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'patients.covid_status', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.covid_status', '=', 'Recovered']
                            ])->get();

        $city_results_deceased = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'patients.covid_status', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.covid_status', '=', 'Deceased']
                            ])->get();
        
        // results for city with barangay                    
        $brgy_results_positive = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'patients.covid_status', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['brgies.id', '=', $brgy_id],
                                ['patients.case_type', '=', 'Positive']
                            ])->get();

        $brgy_results_active = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'patients.covid_status', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['brgies.id', '=', $brgy_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.covid_status', '=', 'Active']
                            ])->get();

        $brgy_results_recovered = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'patients.covid_status', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['brgies.id', '=', $brgy_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.covid_status', '=', 'Recovered']
                            ])->get();

        $brgy_results_deceased = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'patients.covid_status', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['brgies.id', '=', $brgy_id],
                                ['patients.case_type', '=', 'Positive'],
                                ['patients.covid_status', '=', 'Deceased']
                            ])->get();

        return view('covid-report.index')->with([
            'city_request' => $city_request, 
            'brgy_request' => $brgy_request, 
            'cities' => $cities, 
            'brgys' => $brgys, 
            'city_id' => $city_id, 
            'brgy_id' => $brgy_id,
            'city_results_positive' => $city_results_positive,
            'city_results_active' => $city_results_active,
            'city_results_recovered' => $city_results_recovered,
            'city_results_deceased' => $city_results_deceased,
            'brgy_results_positive' => $brgy_results_positive,
            'brgy_results_active' => $brgy_results_active,
            'brgy_results_recovered' => $brgy_results_recovered,
            'brgy_results_deceased' => $brgy_results_deceased
        ]);
    }
}
