<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Brgy;
use App\Patient;

class PatientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // retrieve all patients in all barangays
        $patients = Patient::orderBy('created_at', 'desc')->get();
        $brgys = Brgy::all();
        $cities = City::all();
        return view('patients.index')->with(['patients' => $patients, 'brgys' => $brgys, 'cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // add new patient
        $brgys = Brgy::all();
        $cities = City::all();
        $case_types = ["PUI", "PUM", "Positive", "Negative"];
        $status_list = ["Active", "Recovered", "Deceased"];
        return view('patients.create')->with([
            'brgys' => $brgys,
            'cities' => $cities,
            'case_types' => $case_types, 
            'status_list' => $status_list
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Patient $patient)
    {
        // Form Validation
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required',
            'brgy' => 'required',
            'number' => 'required',
            'email' => "nullable|email|unique:patients,email,{$patient->id}",
            'case_type' => 'required'
        ]);

        // Insert patient to database
        $patient = new Patient;
        $patient->name = $request->input('name');
        $patient->brgy_id = $request->input('brgy');
        $patient->number = $request->input('number');
        $patient->email = $request->input('email');
        $patient->case_type = $request->input('case_type');
        $patient->covid_status = $request->input('covid_status');
        $patient->save();

        return redirect('/patients')->with('success', 'Patient added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // patient single page
        $patient = Patient::find($id);
        $brgy = Patient::find($id)->brgy;
        $brgy_id = Patient::find($id)->brgy_id;
        $city = Brgy::find($brgy_id)->city;

        return view('patients.show')->with(['patient' => $patient, 'brgy' => $brgy, 'city' => $city]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Edit existing patient
        $patient = Patient::find($id);
        $brgys = Brgy::all();
        $cities = City::all();
        $case_types = ["PUI", "PUM", "Positive", "Negative"];
        $status_list = ["Active", "Recovered", "Deceased"];

        return view('patients.edit')->with([
            'patient' => $patient, 
            'brgys' => $brgys, 
            'cities' => $cities, 
            'case_types' => $case_types,
            'status_list' => $status_list
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Form Validation
        $this->validate($request, [
            'name' => 'required',
            'city' => 'required',
            'brgy' => 'required',
            'number' => 'required',
            'email' => "nullable|email|unique:patients,email,{$id}",
            'case_type' => 'required'
        ]);

        // Insert patient to database
        $patient = Patient::find($id);
        $patient->name = $request->input('name');
        $patient->brgy_id = $request->input('brgy');
        $patient->number = $request->input('number');
        $patient->email = $request->input('email');
        $patient->case_type = $request->input('case_type');
        $patient->covid_status = $request->input('covid_status');
        $patient->save();

        return redirect('/patients')->with('success', 'Patient updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // remove patient
        $patient = Patient::find($id);

        $patient->delete();
        return redirect('/patients')->with('success', 'Patient removed successfully.');
    }
}
