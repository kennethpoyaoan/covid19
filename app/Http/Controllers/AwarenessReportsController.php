<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Brgy;
use App\Patient;
use DB;

class AwarenessReportsController extends Controller
{
    // Awareness Reports Views
    public function index(Request $request) {

        $city_id = $request->input('city');
        $brgy_id = $request->input('brgy');

        $brgys = Brgy::all();
        $cities = City::all();
        
        $city_request = City::find($city_id);
        $brgy_request = Brgy::find($brgy_id);

        // results for city only
        $city_results_pui = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'PUI']
                            ])->get();

        $city_results_pum = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'PUM']
                            ])->get();

        $city_results_positive = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Positive']
                            ])->get();

        $city_results_negative = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['patients.case_type', '=', 'Negative']
                            ])->get();
        
        // results for city with barangay                    
        $brgy_results_pui = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['brgies.id', '=', $brgy_id],
                                ['patients.case_type', '=', 'PUI']
                            ])->get();

        $brgy_results_pum = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['brgies.id', '=', $brgy_id],
                                ['patients.case_type', '=', 'PUM']
                            ])->get();

        $brgy_results_positive = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['brgies.id', '=', $brgy_id],
                                ['patients.case_type', '=', 'Positive']
                            ])->get();

        $brgy_results_negative = DB::table('cities')
                            ->join('brgies', 'cities.id', '=', 'brgies.city_id')
                            ->join('patients', 'brgies.id', '=', 'patients.brgy_id')
                            ->select('patients.case_type', 'brgies.city_id', 'brgies.id')
                            ->where([
                                ['brgies.city_id', '=', $city_id],
                                ['brgies.id', '=', $brgy_id],
                                ['patients.case_type', '=', 'Negative']
                            ])->get();

        return view('awareness-report.index')->with([
            'city_request' => $city_request, 
            'brgy_request' => $brgy_request, 
            'cities' => $cities, 
            'brgys' => $brgys, 
            'city_id' => $city_id, 
            'brgy_id' => $brgy_id,
            'city_results_pui' => $city_results_pui,
            'city_results_pum' => $city_results_pum,
            'city_results_positive' => $city_results_positive,
            'city_results_negative' => $city_results_negative,
            'brgy_results_pui' => $brgy_results_pui,
            'brgy_results_pum' => $brgy_results_pum,
            'brgy_results_positive' => $brgy_results_positive,
            'brgy_results_negative' => $brgy_results_negative
        ]);
    }
}