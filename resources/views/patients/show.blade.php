@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <a href="/patients" class="btn btn-light">&#x2190; Back to Patients</a>
            <h1>Patient Information</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <table class="table table-bordered">
                <tr>
                    <th>Name</th>
                    <td>{{ $patient->name }}</td>
                </tr>
                <tr>
                    <th>Brgy</th>
                    <td>{{ $brgy->name }}</td>
                </tr>
                <tr>
                    <th>City</th>
                    <td>{{ $city->name }}</td>
                </tr>
                <tr>
                    <th>Number</th>
                    <td>{{ $patient->number }}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{ $patient->email !== null ? $patient->email : "N/A" }}</td>
                </tr>
                <tr>
                    <th>Case Type</th>
                    <td>{{ $patient->case_type }}</td>
                </tr>
                @if ($patient->case_type === "Positive")
                    <tr>
                        <th>Covid Status</th>
                        <td>{{ $patient->covid_status }}</td>
                    </tr>
                @endif
            </table>
            <a href="/patients/{{ $patient->id }}/edit" class="btn btn-success">Edit Patient</a>
            <form action="/patients/{{ $patient->id }}" method="POST" class="float-right">
                @csrf
                @method('DELETE')
                <input class="btn btn-danger" type="submit" value="Remove Patient" onclick="return confirm('Are you sure you want to remove this patient?');">
            </form>
        </div>
    </div>
@endsection