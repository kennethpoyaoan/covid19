@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Patients</h1>
        </div>
        <div class="col">
            <a href="/patients/create" class="btn btn-primary float-right">Add Patient</a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            @if (count($patients) > 0)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>City</th>
                            <th>Brgy</th>
                            <th>Case Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($patients as $patient)
                            <tr>
                                <td><a href="/patients/{{$patient->id}}">{{$patient->id}}</a></td>
                                <td>{{$patient->name}}</td>
                                @foreach ($brgys as $brgy)
                                    @if ($brgy->id == $patient->brgy_id)
                                        @foreach ($cities as $city)
                                            @if ($brgy->city_id == $city->id)
                                                <td>{{$city->name}}</td>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                                @foreach ($brgys as $brgy)
                                    @if ($brgy->id == $patient->brgy_id)
                                        <td>{{$brgy->name}}</td>
                                    @endif
                                @endforeach
                                <td>{{$patient->case_type}}</td>
                                <td>
                                    <a href="/patients/{{$patient->id}}" class="btn btn-secondary float-left">View</a>
                                    <a href="/patients/{{$patient->id}}/edit" class="btn btn-success float-left">Edit</a>
                                    <form action="/patients/{{ $patient->id }}" method="POST" class="float-left" >
                                        @csrf
                                        @method('DELETE')
                                        <input class="btn btn-danger" type="submit" value="Delete" onclick="return confirm(`Are you sure you want to delete patient '{{ $patient->name }}'?`);">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>No patients found.</p>
            @endif
        </div>
    </div> 
@endsection