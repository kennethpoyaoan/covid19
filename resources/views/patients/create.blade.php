@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Add New Patient</h1>
            <p>Please note that all fields marked with an asterisk (<span class="required">*</span>) are required. </p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <form action="/patients" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Name <span class="required">*</span></label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name of patient">
                </div>
                <div class="form-group">
                    <label for="brgy">City <span class="required">*</span></label>
                    <select class="form-control" id="select-city" name="city">
                        <option value="">--Select City--</option>
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="brgy">Brgy <span class="required">*</span> (Select city first)</label>
                    <select class="form-control" id="select-brgy" name="brgy">
                        <option value="">--Select Brgy--</option>
                        @foreach ($brgys as $brgy)
                            <option city-data-id="{{$brgy->city_id}}" value="{{ $brgy->id }}">{{ $brgy->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="number">Contact No. <span class="required">*</span></label>
                    <input type="text" name="number" class="form-control" id="number" placeholder="Tel. / Mobile / Phone">
                </div>
                <div class="form-group">
                    <label for="email">Email (Optional)</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="email@example.com">
                </div>
                <div class="form-group">
                    <label for="case-type">Case Type <span class="required">*</span></label>
                    <select class="form-control" id="case-type" name="case_type">
                        <option value="">--Select Case Type--</option>
                        @foreach ($case_types as $case_type)
                            <option value="{{$case_type}}">{{$case_type}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group hidden-field">
                    <label for="covid-status">Covid Status <span class="required">*</span></label>
                    <select class="form-control" id="covid-status" name="covid_status">
                        <option value="">--Select Status--</option>
                        @foreach ($status_list as $status)
                            <option value="{{$status}}">{{$status}}</option>
                        @endforeach
                    </select>
                </div>
                <input class="btn btn-primary" type="submit" value="Add"> <a href="/patients" class="btn btn-light">Cancel</a>
            </form>       
        </div>
    </div>
@endsection