@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Cities</h1>
        </div>
        <div class="col">
            <a href="/cities/create" class="btn btn-primary float-right">Add City</a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            @if (count($cities) > 0)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>City</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($cities as $city)
                            <tr>
                                <td><a href="/cities/{{$city->id}}">{{$city->id}}</a></td>
                                <td>{{$city->name}}</td>
                                <td>
                                    <a href="/cities/{{$city->id}}" class="btn btn-secondary float-left">View</a>
                                    <a href="/cities/{{$city->id}}/edit" class="btn btn-success float-left"><i class="fa fa-edit"></i>Edit</a>
                                    <form action="/cities/{{ $city->id }}" method="POST" class="float-left">
                                        @csrf
                                        @method('DELETE')
                                    <input class="btn btn-danger" type="submit" value="Delete" onclick="return confirm(`Are you sure you want to delete city '{{ $city->name }}'?`);">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>No city found.</p>
            @endif
        </div>
    </div>
@endsection