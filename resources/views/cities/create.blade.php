@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Add New City</h1>
            <p>Please note that all fields marked with an asterisk (<span class="required">*</span>) are required. </p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <form action="/cities" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Name <span class="required">*</span></label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name of city">
                </div>
                <input class="btn btn-primary" type="submit" value="Add"> <a href="/cities" class="btn btn-light">Cancel</a>
            </form>
        </div>
    </div>
@endsection