@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <a href="/cities" class="btn btn-light">&#x2190; Back to Cities</a>
            <h1>City Information</h1>
            <hr>
            <h4>{{$city->name}}</h4>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <a href="/cities/{{$city->id}}/edit" class="btn btn-success">Edit City</a>
            <form action="/cities/{{ $city->id }}" method="POST" class="float-right">
                @csrf
                @method('DELETE')
                <input class="btn btn-danger" type="submit" value="Remove City" onclick="return confirm('Are you sure you want to remove this city?');">
            </form>
        </div>
    </div>
@endsection