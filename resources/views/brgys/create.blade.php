@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Add New Barangay</h1>
            <p>Please note that all fields marked with an asterisk (<span class="required">*</span>) are required. </p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <form action="/brgys" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Name <span class="required">*</span></label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name of brgy">
                </div>
                <div class="form-group">
                    <label for="city">City <span class="required">*</span></label>
                    <select class="form-control" id="city" name="city">
                        <option value="">--Select City--</option>
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                    </select>
                </div>
                <input class="btn btn-primary" type="submit" value="Add"> <a href="/brgys" class="btn btn-light">Cancel</a>
            </form>        
        </div>
    </div>
@endsection