@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Update Barangay</h1>
            <p>Please note that all fields marked with an asterisk (<span class="required">*</span>) are required. </p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <form action="/brgys/{{ $brgy->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name <span class="required">*</span></label>
                    <input type="text" name="name" value="{{ $brgy->name }}" class="form-control" id="name" placeholder="Enter name of brgy">
                </div>
                <div class="form-group">
                    <label for="city">City <span class="required">*</span></label>
                    <select class="form-control" id="city" name="city">
                        <option value="">--Select City--</option>
                        @foreach ($cities as $city)
                            @if ($brgy->city_id == $city->id)
                                <option value="{{ $city->id }}" selected="selected">{{ $city->name }}</option>
                            @else
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <input class="btn btn-primary" type="submit" value="Save Changes"> <a href="/brgys" class="btn btn-light">Cancel</a>
            </form>
        </div>
    </div>
@endsection