@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <a href="/brgys" class="btn btn-light">&#x2190; Back to Barangays</a>
            <h1>Barangay Information</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <table class="table table-bordered">
                <tr>
                    <th>Brgy</th>
                    <td>{{ $brgy->name }}</td>
                </tr>
                <tr>
                    <th>City</th>
                    <td>{{ $city->name }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a href="/brgys/{{ $brgy->id }}/edit" class="btn btn-success">Edit Barangay</a>
            <form action="/brgys/{{ $brgy->id }}" method="POST" class="float-right">
                @csrf
                @method('DELETE')
                <input class="btn btn-danger" type="submit" value="Remove Barangay" onclick="return confirm('Are you sure you want to remove this barangay?');">
            </form>
        </div>
    </div>
@endsection