@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Barangays</h1>
        </div>
        <div class="col">
            <a href="/brgys/create" class="btn btn-primary float-right">Add Barangay</a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            @if (count($brgys) > 0)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>City</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($brgys as $brgy)
                            <tr>
                                <td><a href="/brgys/{{$brgy->id}}">{{$brgy->id}}</a></td>
                                <td>{{$brgy->name}}</td>
                                    @foreach ($cities as $city)
                                        @if ($city->id == $brgy->city_id)
                                            <td>{{$city->name}}</td>
                                        @endif
                                    @endforeach
                                <td>
                                    <a href="/brgys/{{$brgy->id}}" class="btn btn-secondary float-left">View</a>
                                    <a href="/brgys/{{$brgy->id}}/edit" class="btn btn-success float-left">Edit</a>
                                    <form action="/brgys/{{ $brgy->id }}" method="POST" class="float-left">
                                        @csrf
                                        @method('DELETE')
                                    <input class="btn btn-danger" type="submit" value="Delete" onclick="return confirm(`Are you sure you want to delete brgy '{{ $brgy->name }}'?`);">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>No barangay found.</p>
            @endif
        </div>
    </div>
@endsection