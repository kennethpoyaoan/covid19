@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Covid19 Report</h1>
            <p>Select City / Brgy to generate report.</p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <form action="" method="GET">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control" name="city" id="select-city">
                                <option value="">--Select City--</option>
                                @foreach ($cities as $city)
                                    @if (!empty($city_id))
                                        @if ($city_id == $city->id)
                                            <option value="{{$city->id}}" selected="selected">{{$city->name}}</option>
                                        @else
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endif
                                    @else
                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control" name="brgy" id="select-brgy">
                                <option value="">--Select Brgy--</option>
                                @foreach ($brgys as $brgy)
                                    @if (!empty($brgy_id))
                                        @if ($brgy_id == $brgy->id)
                                            <option city-data-id="{{$brgy->city_id}}" value="{{$brgy->id}}" selected="selected">{{$brgy->name}}</option>
                                        @else
                                            <option city-data-id="{{$brgy->city_id}}" value="{{$brgy->id}}">{{$brgy->name}}</option>
                                        @endif
                                    @else
                                        <option city-data-id="{{$brgy->city_id}}" value="{{$brgy->id}}">{{$brgy->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <input class="btn btn-primary" type="submit" value="Generate Report" id="generate-report">
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (!empty($city_id))
        @if (!empty($brgy_id))
            <div class="row" style="margin-top: 50px;">
                <div class="col">
                    <h4>{{$city_request->name}} - {{$brgy_request->name}} (Case Reports)</h4>
                    <table class="table table-bordered">
                            <tr>
                                <th>Total number of Persons with Corona Virus (Positive)</th>
                                <td>{{count($brgy_results_positive)}}</td>
                            </tr>
                            <tr>
                                <th>Active</th>
                                <td>{{count($brgy_results_active)}}</td>
                            </tr>
                            <tr>
                                <th>Recovered</th>
                                <td>{{count($brgy_results_recovered)}}</td>
                            </tr>
                            <tr>
                                <th>Deaths</th>
                                <td>{{count($brgy_results_deceased)}}</td>
                            </tr>
                    </table>
                </div>
            </div>
        @else
            <div class="row" style="margin-top: 50px;">
                <div class="col">
                    <h4>{{$city_request->name}} (Case Reports)</h4>
                    <table class="table table-bordered">
                            <tr>
                                <th>Total number of Persons with Corona Virus (Positive)</th>
                                <td>{{count($city_results_positive)}}</td>
                            </tr>
                            <tr>
                                <th>Active</th>
                                <td>{{count($city_results_active)}}</td>
                            </tr>
                            <tr>
                                <th>Recovered</th>
                                <td>{{count($city_results_recovered)}}</td>
                            </tr>
                            <tr>
                                <th>Deaths</th>
                                <td>{{count($city_results_deceased)}}</td>
                            </tr>
                    </table>
                    
                </div>
            </div>
        @endif
    @endif
@endsection