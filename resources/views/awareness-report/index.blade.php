@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Awareness Report</h1>
            <p>Select City / Brgy to generate report.</p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <form action="" method="GET">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control" name="city" id="select-city">
                                <option value="">--Select City--</option>
                                @foreach ($cities as $city)
                                    @if (!empty($city_id))
                                        @if ($city_id == $city->id)
                                            <option value="{{$city->id}}" selected="selected">{{$city->name}}</option>
                                        @else
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endif
                                    @else
                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control" name="brgy" id="select-brgy">
                                <option value="">--Select Brgy--</option>
                                @foreach ($brgys as $brgy)
                                    @if (!empty($brgy_id))
                                        @if ($brgy_id == $brgy->id)
                                            <option city-data-id="{{$brgy->city_id}}" value="{{$brgy->id}}" selected="selected">{{$brgy->name}}</option>
                                        @else
                                            <option city-data-id="{{$brgy->city_id}}" value="{{$brgy->id}}">{{$brgy->name}}</option>
                                        @endif
                                    @else
                                        <option city-data-id="{{$brgy->city_id}}" value="{{$brgy->id}}">{{$brgy->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <input class="btn btn-primary" type="submit" value="Generate Report" id="generate-report">
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (!empty($city_id))
        @if (!empty($brgy_id))
            <div class="row" style="margin-top: 50px;">
                <div class="col">
                    <h4>{{$city_request->name}} - {{$brgy_request->name}} (Case Reports)</h4>
                    <table class="table table-bordered">
                            <tr>
                                <th>PUI</th>
                                <td>{{count($brgy_results_pui)}}</td>
                            </tr>
                            <tr>
                                <th>PUM</th>
                                <td>{{count($brgy_results_pum)}}</td>
                            </tr>
                            <tr>
                                <th>Positive on Covid</th>
                                <td>{{count($brgy_results_positive)}}</td>
                            </tr>
                            <tr>
                                <th>Negative on Covid</th>
                                <td>{{count($brgy_results_negative)}}</td>
                            </tr>
                    </table>
                </div>
            </div>
        @else
            <div class="row" style="margin-top: 50px;">
                <div class="col">
                    <h4>{{$city_request->name}} (Case Reports)</h4>
                    <table class="table table-bordered">
                            <tr>
                                <th>PUI</th>
                                <td>{{count($city_results_pui)}}</td>
                            </tr>
                            <tr>
                                <th>PUM</th>
                                <td>{{count($city_results_pum)}}</td>
                            </tr>
                            <tr>
                                <th>Positive on Covid</th>
                                <td>{{count($city_results_positive)}}</td>
                            </tr>
                            <tr>
                                <th>Negative on Covid</th>
                                <td>{{count($city_results_negative)}}</td>
                            </tr>
                    </table>
                    
                </div>
            </div>
        @endif
    @endif
@endsection