<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Routes
Route::get('/', 'PatientsController@index');

Route::get('/awareness-report', 'AwarenessReportsController@index');
Route::get('/covid19-report', 'CovidReportsController@index');

Route::resource('cities', 'CitiesController');
Route::resource('brgys', 'BrgysController');
Route::resource('patients', 'PatientsController');